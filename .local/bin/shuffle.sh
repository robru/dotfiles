#!/bin/sh

IFS='
'

for filename in $(find -name '*.mp3' | shuf); do
    mplayer "$filename"
done
