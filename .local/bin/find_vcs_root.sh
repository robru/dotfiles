#!/bin/sh -e
#
# This script searches upwards for common VCS directories,
# giving up if it ever leaves $HOME.
#
# Once it finds the vcs, it will execute whatever arguments
# are given to it. The assumption is that the vcs will exist in
# the project root, and there are many things you might want to
# do there, such as `ctags -R` or `make`...

case "$PWD" in
    $HOME*)
        while ! test -d .bzr -o -d .git -o -d .svn -o -d .hg -o -d .cvs; do
            [ "$PWD" = "$HOME" ] && exit 1
            cd ..
        done

        "$@"
        ;;
    *)
        exit 2
        ;;
esac
