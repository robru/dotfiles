#!/bin/sh

notify () {
    play -q "/usr/share/sounds/gnome/default/alerts/$1" &
    [ -f $XDG_RUNTIME_DIR/gnome-shell/*/screenShield.locked ] && \
        echo "$SMUXI_MSG_TIMESTAMP_ISO_LOCAL <$SMUXI_SENDER> $SMUXI_MSG" \
        >> ~/missed-$SMUXI_CHAT_NAME.txt
    exit 0
}

HL_WORDS=$(grep HighlightWords $HOME/.config/smuxi/smuxi-engine.ini | tr -d '\r' | awk '{print $3}')

# Messages to me or mentioning me directly.
echo "$SMUXI_CHAT_TYPE" | egrep -qc "^Person$"                      && notify glass.ogg
echo "$SMUXI_MSG"       | egrep -qc "$SMUXI_PROTOCOL_MANAGER_ME_ID" && notify glass.ogg

# Messages containing my other highlights.
echo "$SMUXI_MSG"       | egrep -qc "$HL_WORDS"                     && notify sonar.ogg
