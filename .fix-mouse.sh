#!/bin/sh

xset dpms 1200
xinput --set-prop 'Logitech USB-PS/2 Optical Mouse' 'Device Accel Constant Deceleration' .3
xinput --set-prop 'Logitech USB-PS/2 Optical Mouse' 'Device Accel Velocity Scaling' 50
