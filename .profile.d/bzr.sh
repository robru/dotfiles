#!/bin/bash

# Behaves more like 'git diff'
function bzrdiff {
    bzr cdiff "$@" | less
}

# Execute commands in bzr tree root and return to starting directory.
function in-bzr-root {
    start=$PWD
    while [[ ! -d .bzr && $PWD != / ]]; do cd ..; done
    "$@"
    cd "$start"
}

# Define $LP_BRANCH as the name of the folder containing the .bzr directory,
# and $LP_PROJECT as the name of the folder containing $LP_BRANCH
function identify-launchpad-branch { in-bzr-root _identify-launchpad-branch; }
function _identify-launchpad-branch {
    export LP_BRANCH=$(basename $PWD)
    LP_BASE=$(basename $(dirname $PWD))
    export LP_PROJECT=$(echo $LP_BASE | cut -d@ -f1)
    export LP_TARGET=$(echo $LP_BASE | tr @ /)
}

# My local branches are stored like ~/src/launchpad-project-name/my-branch-name
# A '@' serves as a sentinel for '/' in the project name, so for example
# ~/src/cordova-ubuntu@2.8/ refers to lp:cordova-ubuntu/2.8
# Usage: `bzrbranch fix-lp-12345678`
function bzrbranch {
    [ $# -eq 0 ] && branch="trunk" || branch=$1
    bzr branch lp:$(basename $PWD | tr @ /) "$branch" && cd -- "$branch"
}

# Push this branch to my Launchpad account.
# Usage: `bzrpush`
function bzrpush {
    identify-launchpad-branch
    bzr push lp:~/$LP_PROJECT/$LP_BRANCH --remember "$@"
}

# Propose a merge on Launchpad
# Usage: `bzrpropose "Fix that bug (LP: #12345678)"`
function bzrpropose {
    identify-launchpad-branch
    if [ $# -eq 0 ]; then
        echo 'Please specify a commit message.'
    else
        yes | BZR_EDITOR=/bin/true bzr lp-propose lp:$LP_TARGET -m "$@"
    fi
}

# Small change? Why not commit, push, and propose it in one step?
# Usage: `bzrcompose "Fix that bug (LP: #12345678)"`
function bzrcompose {
    if [ $# -eq 0 ]; then
        echo 'Please specify a commit message.'
    else
        bzr commit -m "$@"
        bzrpush
        bzrpropose "$@"
    fi
}

# Fetch trunk for a project I don't yet have locally.
# Usage: `cd ~/src/; bzrstart unity`
function bzrstart {
    mkcd "$1"
    bzrbranch
}

# Build and install all binary packages defined by this project.
# Usage: `bzrbd`
function bzrbd { in-bzr-root _bzrbd; }
function _bzrbd {
    rm -rvf ../*.deb ../build-area/ ../*orig.tar.gz | tail
    bzr bd "$@"
    sudo dpkg -i ../build-area/*deb
}
