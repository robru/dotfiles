#!/bin/bash

# Create and cd into a directory in one step.
# Usage: `mkcd foobar`
function mkcd {
    mkdir -pv -- "$1" && cd -- "$1"
}

# Reload bashrc, easier to type during development.
# Usage: `bashreload`
function bashreload {
    . ~/.bashrc
}
