#!/bin/bash

# Execute commands in bzr tree root and return to starting directory.
function in-git-root {
    start=$PWD
    while [[ ! -d .git && $PWD != / ]]; do cd ..; done
    "$@"
    cd "$start"
}

# Switch to master, discard changes, then merge in upstream.
# Usage: `gitupstream`
function gitupstream { in-git-root _gitupstream; }
function _gitupstream {
    git checkout master
    git checkout .
    git fetch upstream
    git merge upstream/master
}
