" Pathogen Boilerplate {{{
set nocompatible
let g:bundle_path='~/.config/nvim/bundle/'

function! Require(bundle) abort
    let l:package = split(a:bundle, "/")[-1]
    if !isdirectory(expand(g:bundle_path . l:package))
        exe "!git clone " . a:bundle . " " . g:bundle_path . l:package
    endif
endfunction

" The man... the legend...
call Require('https://github.com/tpope/vim-pathogen')
call Require('https://github.com/tpope/vim-commentary')
call Require('https://github.com/tpope/vim-unimpaired')
call Require('https://github.com/tpope/vim-repeat')
call Require('https://github.com/tpope/vim-jdaddy')
call Require('https://github.com/tpope/vim-surround')
call Require('https://github.com/tpope/vim-sensible')

" Misc things I like.
call Require('https://github.com/airblade/vim-gitgutter')
call Require('https://github.com/justinmk/vim-sneak')
call Require('https://github.com/chrisbra/SudoEdit.vim')
call Require('https://github.com/ctrlpvim/ctrlp.vim')
call Require('https://github.com/neomake/neomake')

" Appearance
call Require('https://github.com/flazz/vim-colorschemes')
call Require('https://github.com/vim-airline/vim-airline')

" File formats...
call Require('https://github.com/leafgarland/typescript-vim.git')
call Require('https://github.com/pangloss/vim-javascript')
call Require('https://github.com/hylang/vim-hy')
call Require('https://github.com/dag/vim-fish')

runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect(g:bundle_path . "{}")
" }}}

" Settings {{{
" Wrap
set wrap
set linebreak
set breakindent
set sidescroll=5

" Indentation
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set shiftround
set cindent

" VCS
set updatetime=1000
set nobackup
set nowritebackup
set noswapfile
set autowriteall
set hidden
set shell=/bin/sh

" Search
set ignorecase
set smartcase
set showmatch
set hlsearch
set gdefault

" Clipboard
set clipboard=unnamed
if has('unnamedplus')
  set clipboard+=unnamedplus
endif

" Cursor
set virtualedit=block
set scrolloff=10
" }}}

" Sneak {{{
let g:sneak#label = 1
let g:sneak#use_ic_scs = 1
map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T
map <expr> j !v:count ? '<esc>' : 'j'
map <expr> k !v:count ? '<esc>' : 'k'
" }}}

" Handy Mappings {{{
let mapleader="\\"

" Power Saving
cnoremap w!! SudoWrite<cr>

" Clear match highlighting
noremap <leader><space> :nohlsearch<cr>:call clearmatches()<cr>

" Consistency is key
noremap Y y$
xnoremap iz :<C-u>silent!normal![zV]z<cr>
onoremap iz :normal viz<cr>
" }}}

" Modifier Avoidance {{{
inoremap jk <esc>
noremap <leader><leader> :bnext<cr>
noremap <leader>p :CtrlP<cr>
noremap <leader>w :w!<cr>
noremap <leader>q :wq!<cr>
noremap <space> @q

" Don't mill around in insert mode
noremap <down> 10<C-d>
noremap <up> 10<C-u>
inoremap <down> <esc>10<C-d>
inoremap <up> <esc>10<C-u>
inoremap <left> <esc>
inoremap <right> <esc>ll
" }}}

" Poor Man's Vim Plugins {{{
" In this section I reimplement some existing vim plugins in a simpler way,
" because I appreciate simplicity.

" Poor Man's Cursor Position Restoration
set viminfo='50,\"100,:20,%,n~/.viminfo
augroup poor_man
    autocmd!
    " g`" means "Go to the last mark" and zv means "unfold until cursor visible"
    autocmd BufReadPost * silent! normal! g`"zv

    " Poor Man's InsertLeave (put the cursor where I expect it to be)
    autocmd InsertLeave * :normal `^

    " Poor Man's Auto Save
    autocmd TextChanged,InsertLeave * if !&readonly | update | endif
    autocmd CursorHold,TextChanged * checktime
    autocmd BufWritePost * Neomake

    " Poor Man's Markdown
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown
augroup END

" Poor Man's Ctags
set tags=./.tags;
silent! !~/.local/bin/rtags
" }}}

" View Splitting {{{
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" }}}

" Whitespace Eradication {{{
function! DecideWhetherToStrip()
    let l:search = @/
    " Check if a file being opened has trailing spaces in the first place.
    let b:dostrip = !search('\s\+$', 'npw')
    let @/ = l:search
endfunction

function! StripTrailingWhitespace()
    if !exists('b:dostrip')
        let b:dostrip = 0
    endif
    if b:dostrip
        let l:search = @/
        let l:winview = winsaveview()
        %s/\s\+$//e            " Kill spaces at EOL
        %s/\($\n\s*\)\+\%$//e  " Kill lines at EOF
        let @/ = l:search
        call winrestview(l:winview)
    endif
endfunction

set list listchars=tab:»·,trail:·
augroup whitespace
    autocmd!
    autocmd BufReadPost * call DecideWhetherToStrip()
    autocmd BufWritePre,TextChanged,InsertLeave * call StripTrailingWhitespace()
augroup END
" }}}

" Visual Appearance {{{
colorscheme CandyPaper
let g:airline#extensions#tabline#enabled = 1

set number
set relativenumber

augroup insertmode
    autocmd!
    autocmd InsertEnter * set   cursorcolumn   cursorline norelativenumber colorcolumn=80,81,82
    autocmd InsertLeave * set nocursorcolumn nocursorline   relativenumber colorcolumn=
augroup END
" }}}
