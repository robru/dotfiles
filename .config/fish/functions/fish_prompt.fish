set -g hostname (hostname|cut -d . -f 1)


set -g black  (set_color -o black)
set -g green  (set_color -o green)
set -g red    (set_color -o red)
set -g yellow (set_color -o yellow)
set -g blue   (set_color -o blue)
set -g normal (set_color normal)


function fish_prompt --description "Prompt user for a command to run."
    set -g __pwd (echo $PWD | sed "s#$HOME#~#")
    echo -s "$green$USER$black at $red$hostname$black in $yellow$__pwd$black on $blue" (vcprompt.py)
    echo "\$ $normal"
end
