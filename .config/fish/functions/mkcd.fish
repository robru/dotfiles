function mkcd --description 'Make a directory then cd into it.'
    mkdir -pv -- $argv
    and cd $argv
end
