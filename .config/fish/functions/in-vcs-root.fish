function in-vcs-root --description 'Execute a command in the vcs root dir.'
    pushd $PWD
    while not test -d .bzr -o -d .git -o -d .svn -o -d .hg -o -d .cvs -o $PWD = /
        cd ..
    end
    eval $argv
    popd
end
