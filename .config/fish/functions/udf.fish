function udf --description 'Flash ubuntu onto a phone.'
    adb reboot-bootloader
    ubuntu-device-flash touch --channel=$FLASH_CHANNEL --developer-mode --wipe --bootstrap --password qwerty --device=krillin
end
