function _gitupstream
    git checkout master
    git checkout .
    git fetch upstream
    git merge upstream/master
end

function gitupstream --description 'Pull remote upstream and merge into local master.'
    in-vcs-root _gitupstream
end
