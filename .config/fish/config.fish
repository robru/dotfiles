alias a 'atom .'
alias v 'nvim'
alias n 'nvim'

# Some random environment variables.
set -gx EDITOR 'nano'
set -gx VISUAL 'nano'
set -gx SHELL /usr/bin/fish
set -gx GOPATH /tmp/go
set -gx TERM screen-256color

# This makes less always behave like 'git diff' does.
set -gx LESS 'FREESEX'

set -U fish_user_paths ~/.local/bin
set fish_greeting ""
