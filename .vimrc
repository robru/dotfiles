" Pathogen Boilerplate {{{
set nocompatible

" This function will install any plugin that is missing.
function! Fetch(bundle) abort
    let l:package=split(a:bundle, "/")[1]
    if !isdirectory(expand('~/.vim/bundle/' . l:package))
        echom "Installing " . l:package . "..."
        echom ""
        silent! !mkdir -p ~/.vim/bundle/
        exe "silent! !git clone https://github.com/".a:bundle." ~/.vim/bundle/".l:package
    endif
endfunction

command! -nargs=1 Bundle :call Fetch(<args>)

" The man... the legend...
Bundle 'tpope/vim-pathogen'
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-unimpaired'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-jdaddy'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-sensible'

" Misc things I like.
Bundle 'easymotion/vim-easymotion'
Bundle 'flazz/vim-colorschemes'
Bundle 'kien/ctrlp.vim'
Bundle 'scrooloose/syntastic'
Bundle 'majutsushi/tagbar'
Bundle 'rking/ag.vim'

" File formats...
Bundle 'pangloss/vim-javascript'
Bundle 'dag/vim-fish'

runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()
" }}}

" Settings {{{
" Indentation
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set shiftround
set cindent

" VCS
set updatetime=1000
set nobackup
set nowritebackup
set noswapfile
set autowriteall
set hidden
set shell=/bin/sh

" Search
set ignorecase
set smartcase
set showmatch
set hlsearch
set gdefault

" Clipboard
set clipboard=unnamed
if has('unnamedplus')
  set clipboard+=unnamedplus
endif

" Cursor
set virtualedit=block
set scrolloff=10
" }}}

" EasyMotion {{{
let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1
let g:EasyMotion_keys = 'abcdefghijklmnopqrstuvwxyz1234567890;,.`/-=[]'
map s <Plug>(easymotion-s)
map <expr> w  !v:count ?   '<Plug>(easymotion-w)'            : 'w'
map <expr> b  !v:count ?   '<Plug>(easymotion-b)'            : 'b'
map <expr> e  !v:count ?   '<Plug>(easymotion-e)'            : 'e'
map <expr> ge !v:count ?   '<Plug>(easymotion-ge)'           : 'ge'
map <expr> h  !v:count ? 'zv<Plug>(easymotion-linebackward)' : 'h'
map <expr> l  !v:count ? 'zv<Plug>(easymotion-lineforward)'  : 'l'
map <expr> j  !v:count ? 'zv<Plug>(easymotion-j)'            : 'j'
map <expr> k  !v:count ? 'zv<Plug>(easymotion-k)'            : 'k'
" }}}

" Handy Mappings {{{
let mapleader=","

" Power Saving
cnoremap w!! write !sudo tee % >/dev/null<cr>

" Easy .vimrc futzing
noremap <leader>c :split $MYVIMRC<cr>
noremap <leader>x :source $MYVIMRC<cr>

" Clear match highlighting
noremap <leader><space> :nohlsearch<cr>:call clearmatches()<cr>

" Consistency is key
noremap Y y$
xnoremap iz :<C-u>silent!normal![zV]z<cr>
onoremap iz :normal viz<cr>
" }}}

" Modifier Avoidance {{{
inoremap jk <esc>
inoremap ZZ <esc>ZZ
inoremap ZX <esc>ZX
noremap - $
noremap 0 ^
noremap <leader><leader> :bnext<cr>
noremap ; :
noremap <leader>p :CtrlP<cr>
noremap <space> @q
noremap <F6> :TagbarToggle<CR>
noremap <F7> :!clear;<space>
noremap <F8> :!clear;<space>make<cr>
noremap ZX :w!<cr>

" Learn the new keys
noremap $ <nop>
noremap : <nop>

" Don't mill around in insert mode
noremap <down> 10<C-d>
noremap <up> 10<C-u>
inoremap <down> <esc>10<C-d>
inoremap <up> <esc>10<C-u>
inoremap <left> <esc>
inoremap <right> <esc>ll
" }}}

" Poor Man's Vim Plugins {{{
" In this section I reimplement some existing vim plugins in a simpler way,
" because I appreciate simplicity.

" Poor Man's Cursor Position Restoration
set viminfo='50,\"100,:20,%,n~/.viminfo
augroup poor_man
    autocmd!
    " g`" means "Go to the last mark" and zv means "unfold until cursor visible"
    autocmd BufReadPost * silent! normal! g`"zv

    " Poor Man's InsertLeave (put the cursor where I expect it to be)
    autocmd InsertLeave * :normal `^

    " Poor Man's Auto Save
    autocmd TextChanged,InsertLeave * silent! wall
    autocmd CursorHold,TextChanged * checktime
    autocmd CursorHold,InsertLeave * SyntasticCheck

    " Poor Man's Markdown
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown
augroup END

" Poor Man's Ctags
set tags=./.tags;
silent! !find_vcs_root.sh ctags --excmd=number --recurse -f .tags 2>/dev/null
" }}}

" View Splitting {{{
noremap <leader>v :execute "rightbelow vsplit " . bufname('#')<cr>
noremap <leader>s :execute "rightbelow  split " . bufname('#')<cr>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" }}}

" Whitespace Eradication {{{
function! DecideWhetherToStrip()
    let l:search = @/
    " Check if a file being opened has trailing spaces in the first place.
    let b:dostrip = !search('\s\+$', 'npw')
    let @/ = l:search
endfunction

function! StripTrailingWhitespace()
    if !exists('b:dostrip')
        let b:dostrip = 0
    endif
    if b:dostrip
        let l:search = @/
        let l:winview = winsaveview()
        %s/\s\+$//e            " Kill spaces at EOL
        %s/\($\n\s*\)\+\%$//e  " Kill lines at EOF
        let @/ = l:search
        call winrestview(l:winview)
    endif
endfunction

set list listchars=tab:»·,trail:·
augroup whitespace
    autocmd!
    autocmd BufReadPost * call DecideWhetherToStrip()
    autocmd BufWritePre,TextChanged,InsertLeave * call StripTrailingWhitespace()
augroup END
" }}}

" Folding {{{
set foldlevelstart=2
set foldminlines=15
set foldmethod=syntax
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType python setlocal foldmethod=indent
augroup END
" }}}

" Syntastic {{{
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = '/usr/bin/python3'
let g:syntastic_aggregate_errors = 1
let g:syntastic_python_checkers = ['flake8', 'pep8', 'python']
" }}}

" Tagbar {{{
let g:tagbar_show_linenumbers = -1
let g:tagbar_autoclose = 1
let g:tagbar_autopreview = 1
" }}}

" Visual Appearance {{{
colorscheme jellybeans

set number
set relativenumber
set statusline=
set statusline+=%1*\ \ %<%F\ \              " Path to file
set statusline+=%2*\ \ %y\ \                " Type of file
set statusline+=%3*%=%m%r%w\ \              " Modified? Readonly?
set statusline+=%4*\ \ c:%v\ \              " Cursor column
set statusline+=%5*\ \ %P\ of\ %L\ \        " Position of total lines.

highlight LineNr       ctermbg=233
highlight CursorLineNr ctermbg=233
highlight User1 cterm=reverse,bold ctermfg=202  guifg=#ffdad8  guibg=#880c0e
highlight User2 cterm=reverse,bold ctermfg=208  guifg=#000000  guibg=#F4905C
highlight User3 cterm=reverse,bold ctermfg=214  guifg=#292b00  guibg=#f4f597
highlight User4 cterm=reverse,bold ctermfg=220  guifg=#112605  guibg=#aefe7B
highlight User5 cterm=reverse,bold ctermfg=226  guifg=#051d00  guibg=#7dcc7d
highlight StatusLineNC cterm=reverse
highlight IncSearch                ctermfg=0    ctermbg=226

highlight CursorLine   cterm=none ctermbg=233 guibg=#181818
highlight CursorColumn cterm=none ctermbg=233 guibg=#181818
highlight ColorColumn             ctermbg=235 guibg=#2d2d2d
augroup insertmode
    autocmd!
    autocmd InsertEnter * set nocursorcolumn   cursorline norelativenumber colorcolumn=80,81,82
    autocmd InsertLeave * set nocursorcolumn nocursorline   relativenumber colorcolumn=
augroup END
" }}}
